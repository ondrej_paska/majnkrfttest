﻿using UnityEngine;

[CreateAssetMenu(fileName = "BlockType", menuName = "BlockType/BlockTypesList")]
public class BlockTypesList : ScriptableObject
{
	public BlockType[] types;
	public BlockType grass;
	public BlockType dirt;
	public BlockType wood;
	public BlockType rock;

	void OnEnable()
	{
		for (var i = 0; i < types.Length; i++)
		{
			types[i].ID = (byte) (i + 1);
		}
	}

	public BlockType FromID(int id)
	{
		return types[id - 1];
	}
}