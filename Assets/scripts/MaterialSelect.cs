﻿using System;
using UnityEngine;

/**
 * This script handles the picking of build material
 */
public class MaterialSelect : MonoBehaviour
{
	[SerializeField] private GameObject[] _icons;
	[SerializeField] private BlockTypesList _types;
	[SerializeField] private GameObject _selector;

	private float _selectedValue;
	private int _index;

	public BlockType Selected
	{
		get
		{
			if (_index == 0) return null;
			return _types.types[_index - 1];
		}
	}

	public bool IsBuildEnabled()
	{
		return _index > 0;
	}

	void Awake()
	{
		_selector.transform.position = _icons[0].transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		var wheelValue = Input.GetAxis("Mouse ScrollWheel");
		if (Math.Abs(wheelValue) > 0.04f)
		{
			_selectedValue += wheelValue;
			_selectedValue = Mathf.Clamp(_selectedValue, 0f, _icons.Length - 0.1f);
			_index = Mathf.FloorToInt(_selectedValue) % _icons.Length;
			_selector.transform.position = _icons[_index].transform.position;
		}
	}
}