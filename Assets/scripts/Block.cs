﻿using UnityEngine;

public class Block : MonoBehaviour {

	[SerializeField] private BlockType _type;

	public BlockType Type
	{
		get { return _type; }
		set
		{
			_type = value;
			GetComponent<MeshRenderer>().material = _type.Material;
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
}
