﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WorldManager : MonoBehaviour
{
	//WORLD SETTINGS
	[SerializeField] private int _areaSide = 40; //size of the area square
	[SerializeField] private int _areaHeight = 25; //Max height of area
	[SerializeField] private int _terrainHeight; //Height of the generated terrain
	[SerializeField] private int _loadDistance = 65; //How distant areas should be loaded

	//REFERENCES
	[SerializeField] private Transform _player;
	[SerializeField] private MaterialSelect _materialSelector;
	[SerializeField] private GameObject _areaPrefab;

	//UI REFERENCES
	[SerializeField] private Text _infoText;
	[SerializeField] private Image _overlay;

	//PRIVATE
	private int N; //short for _areaSize

	private string _saveFilePath;
	private Dictionary<int, Area> _areas = new Dictionary<int, Area>();

	void Start()
	{
		_saveFilePath = Path.Combine(Application.persistentDataPath, "save_slot1.dat");

		N = _areaSide;
		Area.N = _areaSide;
		Area.H = _areaHeight;
		Area.TerrainHeight = _terrainHeight;
		Area.PerlinOffset = new Vector2(Random.Range(0f, 99999f), Random.Range(0f, 99999f));

		_player.transform.position = Vector3.up * (_terrainHeight + 10f);

		AddArea(0, 0);
		StartCoroutine("UpdateAreas");

		_infoText.enabled = false;
		_overlay.enabled = true;
		_overlay.DOFade(0, 1.5f).OnComplete(() => _overlay.enabled = false); //Hide the loading stutter
	}

	private Area AddArea(int x, int z, bool init = true)
	{
		var position = new Vector3(N * x, 0, N * z);
		var areaObject = Instantiate(_areaPrefab, position, Quaternion.identity);
		var area = areaObject.GetComponent<Area>();
		area.X = x;
		area.Z = z;
		_areas[EncodePosition(x, z)] = area;
		if (init) area.GenerateTerrain();
		return area;
	}

	public bool IsBuildEnabled()
	{
		return _materialSelector.IsBuildEnabled();
	}

	public void AddBlock(Vector3 position)
	{
		GetArea(position).AddBlock(position, _materialSelector.Selected);
	}

	private Area GetArea(Vector3 p)
	{
		var targetX = Mathf.FloorToInt(p.x / N);
		var targetZ = Mathf.FloorToInt(p.z / N);
		var ps = EncodePosition(targetX, targetZ);
		if (_areas.ContainsKey(ps))
		{
			return _areas[ps];
		}
		throw new Exception("Cannot find area " + targetX + "," + targetZ);
	}

	private static int EncodePosition(int x, int z)
	{
		return x * 10000 + z;
	}

	private IEnumerator UpdateAreas()
	{
		while (true)
		{
			var p = _player.position;
			p.y = 0;

			var areaCenter = new Vector3(N / 2f, 0, N / 2f); //to measure distance to the center of a tile

			//1. Unload areas further away
			foreach (var item in _areas)
			{
				var area = item.Value;
				if (area.Loaded == false)
				{
					continue;
				}
				var dist = Vector3.Distance(area.transform.position + areaCenter, p);
				if (dist > _loadDistance)
				{
					area.Unload();
				}
			}

			//2. Activate or build new areas 
			var loadDistanceInAreas = Mathf.CeilToInt(_loadDistance / (float) N);
			var currentArea = GetArea(p);
			for (int i = -loadDistanceInAreas; i <= loadDistanceInAreas; i++)
			{
				for (int j = -loadDistanceInAreas; j <= loadDistanceInAreas; j++)
				{
					var areaX = currentArea.X + i;
					var areaZ = currentArea.Z + j;
					var areaPos = new Vector3(areaX * N, 0, areaZ * N) + areaCenter;
					if (Vector3.Distance(areaPos, p) < _loadDistance)
					{
						LoadOrBuild(areaX, areaZ);
						yield return null;
					}
				}
			}

			yield return new WaitForSeconds(1.0f);
		}
	}

	private void LoadOrBuild(int x, int z)
	{
		var key = EncodePosition(x, z);
		Area a;
		if (_areas.TryGetValue(key, out a))
		{
			if (a.Loaded == false)
			{
				a.Reload();
			}
			return;
		}
		AddArea(x, z);
	}

	private void Update()
	{
		if (Input.GetButtonDown("Save Game"))
		{
			StartCoroutine("SaveGameRoutine");
		}

		if (Input.GetButtonDown("Load Game"))
		{
			StartCoroutine("LoadGameRoutine");
		}
	}

	private IEnumerator SaveGameRoutine()
	{
		_infoText.enabled = true;
		_infoText.text = "Saving";
		yield return null;

		var areas = new List<AreaDataForSave>();
		foreach (var item in _areas)
		{
			areas.Add(item.Value.GetSaveData());
		}
		//TODO: save only modified areas

		var playerPos = _player.transform.position;

		var saveData = new GameSave
		{
			Areas = areas,
			TerrainOffsetX = Area.PerlinOffset.x,
			TerrainOffsetY = Area.PerlinOffset.y,
			PlayerX = playerPos.x,
			PlayerZ = playerPos.z
		};

		BinaryFormatter formatter = new BinaryFormatter();
		FileStream file = File.Create(_saveFilePath);

		print("Saving game to " + _saveFilePath);

		formatter.Serialize(file, saveData);
		file.Close();

		_infoText.text = "Saved";

		yield return new WaitForSeconds(2f);
		_infoText.enabled = false;
	}

	private IEnumerator LoadGameRoutine()
	{
		_infoText.enabled = true;
		_infoText.text = "Loading";
		yield return null;


		if (!File.Exists(_saveFilePath))
		{
			_infoText.text = "Not data found";
			yield return new WaitForSeconds(2f);
			_infoText.enabled = false;
			yield break;
		}

		_overlay.enabled = true;
		_overlay.color = Color.black;
		_overlay.DOFade(0, 1.5f).SetDelay(0.5f).OnComplete(() => _overlay.enabled = false);

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(_saveFilePath, FileMode.Open);
		var savedData = (GameSave) bf.Deserialize(file);
		file.Close();

		yield return null;

		Area.PerlinOffset.x = savedData.TerrainOffsetX;
		Area.PerlinOffset.y = savedData.TerrainOffsetY;

		//Destroy existing areas
		foreach (var item in _areas)
		{
			Destroy(item.Value.gameObject);
		}
		_areas.Clear();

		print("Loading " + savedData.Areas.Count + " areas");

		foreach (var sA in savedData.Areas)
		{
			var area = AddArea(sA.X, sA.Z, false);
			area.SetBlockData(sA.blocks);
		}
		_player.transform.position = new Vector3(savedData.PlayerX, _terrainHeight + 10f, savedData.PlayerZ);
		StopCoroutine("UpdateAreas");
		StartCoroutine("UpdateAreas"); //to trigger the load of relevent areas

		yield return new WaitForSeconds(2f);
		_infoText.enabled = false;
	}
}