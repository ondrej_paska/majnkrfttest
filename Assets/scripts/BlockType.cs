﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "BlockType/BlockType")]
public class BlockType : ScriptableObject
{
	public float TimeToMine;
	public Material Material;
	public byte ID;
}