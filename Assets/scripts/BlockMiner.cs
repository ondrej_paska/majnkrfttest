﻿using System;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class BlockMiner : MonoBehaviour
{
	[SerializeField] private int _mineReach;
	[SerializeField] private BlockTypesList _types;
	[SerializeField] private LayerMask _ground;
	[SerializeField] private ParticleSystem _particles;
	[SerializeField] private WorldManager _manager;

	private float _timeCounter; //measures time already mined on current block
	private Vector3? _currentBlock;

	private Camera _camera;

	void Start()
	{
		_camera = GetComponentInChildren<Camera>();
	}

	void Update()
	{
		if (!Input.GetButton("Mine"))
		{
			_currentBlock = null;
			return;
		}
		
		Ray ray = _camera.ViewportPointToRay(Vector3.one/2f);
		RaycastHit hit;
		if (!Physics.Raycast(ray, out hit, _mineReach, _ground))
		{
			return;
		}
		var area = hit.collider.GetComponent<Area>();

		if (area == null) return;

		var p = hit.point - hit.normal * 0.5f - area.transform.position;
		p = p.Floor();
		var b = area.GetBlock((int) p.x, (int) p.y, (int) p.z);
		
		if (b == 0) return;

		var type = _types.FromID(b);

		if (p == _currentBlock)
		{
			_timeCounter += Time.deltaTime;
			if (_timeCounter > type.TimeToMine)
			{
				area.RemoveBlock((int) p.x, (int) p.y, (int) p.z);
				_currentBlock = null;
				_timeCounter = 0;
			}
		}
		else
		{
			_currentBlock = p;
			_timeCounter = 0;
		}
		_particles.transform.position = hit.point;
		_particles.Emit(1);
	}
}