﻿using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
	public static int H; //height of an area
	public static int N; //width of an area
	public static int TerrainHeight;
	public static Vector2 PerlinOffset;

	public BlockTypesList Types;
	public int X;
	public int Z;

	private byte[,,] _blocks;
	
	private Mesh _areaMesh;
	private MeshRenderer _meshRenderer;
	private MeshCollider _meshCollider;
	private MeshFilter _meshFilter;

	private bool _loaded;
	public bool Loaded
	{
		get { return _loaded; }
	}

	private void InitFields()
	{
		_meshRenderer = GetComponent<MeshRenderer>();
		_meshCollider = GetComponent<MeshCollider>();
		_meshFilter = GetComponent<MeshFilter>();

		_blocks = new byte[N, H, N];
	}

	public void GenerateTerrain()
	{
		InitFields();

		_loaded = true;

		var p = transform.position;

		for (var i = 0; i < N; i++)
		{
			for (var j = 0; j < N; j++)
			{
				var h = TerrainFormula(i + p.x, j + p.z);
				//two layers of rocks at the bottom
				_blocks[i, 0, j] = Types.rock.ID;
				_blocks[i, 1, j] = Types.rock.ID;

				//fill with dirt
				int c;
				for (c = 2; c < h; c++)
				{
					_blocks[i, c, j] = Types.dirt.ID;
				}
				//grass on top
				_blocks[i, c, j] = Types.grass.ID;
			}
		}

		MakeMesh();
	}

	private static float TerrainFormula(float x, float y)
	{
		const float scale = 1 / 20f;
		var offsetX = PerlinOffset.x;
		var offsetY = PerlinOffset.y;
		return TerrainHeight * Mathf.PerlinNoise(offsetX + x * scale, offsetY + y * scale) - 2f;
	}

	private void MakeMesh()
	{
		_areaMesh = new Mesh();

		var verts = new List<Vector3>();
		var uvs = new List<Vector2>();
		var tris = new List<int>();

		for (var x = 0; x < N; x++)
		{
			for (var y = 0; y < H; y++)
			{
				for (var z = 0; z < N; z++)
				{
					var brick = _blocks[x, y, z];
					if (brick == 0) continue;

					// Left wall
					if (IsTransparent(x - 1, y, z))
						BuildFace(brick, new Vector3(x, y, z), Vector3.up, Vector3.forward, false, verts, uvs, tris);
					// Right wall
					if (IsTransparent(x + 1, y, z))
						BuildFace(brick, new Vector3(x + 1, y, z), Vector3.up, Vector3.forward, true, verts, uvs, tris);

					// Bottom wall
					if (IsTransparent(x, y - 1, z))
						BuildFace(brick, new Vector3(x, y, z), Vector3.forward, Vector3.right, false, verts, uvs, tris);
					// Top wall
					if (IsTransparent(x, y + 1, z))
						BuildFace(brick, new Vector3(x, y + 1, z), Vector3.forward, Vector3.right, true, verts, uvs, tris);

					// Back
					if (IsTransparent(x, y, z - 1))
						BuildFace(brick, new Vector3(x, y, z), Vector3.up, Vector3.right, true, verts, uvs, tris);
					// Front
					if (IsTransparent(x, y, z + 1))
						BuildFace(brick, new Vector3(x, y, z + 1), Vector3.up, Vector3.right, false, verts, uvs, tris);
				}
			}
		}

		_areaMesh.vertices = verts.ToArray();
		_areaMesh.triangles = tris.ToArray();
		_areaMesh.uv = uvs.ToArray();
		_areaMesh.RecalculateBounds();
		_areaMesh.RecalculateNormals();

		_meshFilter.mesh = _areaMesh;
		_meshCollider.sharedMesh = _areaMesh;
	}

	private void BuildFace(byte brick, Vector3 corner, Vector3 up, Vector3 right, bool reversed,
		List<Vector3> verts, List<Vector2> uvs, List<int> tris)
	{
		int index = verts.Count;

		//Vertices
		verts.Add(corner);
		verts.Add(corner + up);
		verts.Add(corner + up + right);
		verts.Add(corner + right);

		//Triangles
		if (reversed)
		{
			tris.Add(index + 0);
			tris.Add(index + 1);
			tris.Add(index + 2);
			tris.Add(index + 2);
			tris.Add(index + 3);
			tris.Add(index + 0);
		}
		else
		{
			tris.Add(index + 1);
			tris.Add(index + 0);
			tris.Add(index + 2);
			tris.Add(index + 3);
			tris.Add(index + 2);
			tris.Add(index + 0);
		}

		//UVs
		var b = brick - 1;
		const float uvWidth = 0.5f;
		const float edge = 0.02f;
		var uvx = (b % 2) * uvWidth;
		var uvy = (int) (b / 2f) * uvWidth;
		uvs.Add(new Vector2(uvx+edge, uvy+edge));
		uvs.Add(new Vector2(uvx+edge, uvy + uvWidth-edge));
		uvs.Add(new Vector2(uvx + uvWidth-edge, uvy + uvWidth-edge));
		uvs.Add(new Vector2(uvx + uvWidth-edge, uvy+edge));
	}

	private bool IsTransparent(int x, int y, int z)
	{
		if (y < 0) return false;
		return GetBlock(x, y, z) == 0;
	}

	public byte GetBlock(int x, int y, int z)
	{
		if (x < 0 || y < 0 || z < 0 || y >= H || x >= N || z >= N) return 0;
		return _blocks[x, y, z];
	}

	private void SetBlock(byte blockId, int x, int y, int z)
	{
		if (x < 0 || y < 0 || z < 0 || y >= H || x >= N || z >= N) return;
		if (_blocks[x, y, z] == blockId)
		{
			return;
		}
		_blocks[x, y, z] = blockId;
		MakeMesh();
	}

	public void AddBlock(Vector3 worldPos, BlockType type)
	{
		var lpos = worldPos - transform.position; //convert to local position
		SetBlock(type.ID, Mathf.FloorToInt(lpos.x), Mathf.FloorToInt(lpos.y), Mathf.FloorToInt(lpos.z));
	}

	public void RemoveBlock(int x, int y, int z)
	{
		_blocks[x, y, z] = 0;
		MakeMesh();
	}

	public void Unload()
	{
		_loaded = false;
		_meshRenderer.enabled = false;
		_meshCollider.enabled = false;
	}

	public void Reload()
	{
		_loaded = true;
		_meshRenderer.enabled = true;
		_meshCollider.enabled = true;
	}

	public AreaDataForSave GetSaveData()
	{
		return new AreaDataForSave(_blocks, X, Z);
	}

	public void SetBlockData(byte[,,] data)
	{
		InitFields();
		_blocks = data;
		MakeMesh();
	}
}