﻿using UnityEngine;

public class BlockBuilder : MonoBehaviour
{
	[SerializeField] private LayerMask _ground;
	[SerializeField] private int _buildReach;
	[SerializeField] private GameObject _blockPreview;
	[SerializeField] private WorldManager _manager;
	private Camera _camera;

	void Start()
	{
		_camera = GetComponentInChildren<Camera>();
	}

	void Update()
	{
		Ray ray = _camera.ViewportPointToRay(Vector3.one / 2f);
		RaycastHit hit;
		if (!_manager.IsBuildEnabled() || !Physics.Raycast(ray, out hit, _buildReach, _ground))
		{
			HidePreview();
			return;
		}

		var area = hit.collider.GetComponent<Area>();

		if (area == null) return;
		
		var blockPos = (hit.point - hit.normal * 0.5f).Floor();
		var targetPosition = blockPos + hit.normal;
		
		if (Vector3.Distance(targetPosition+Vector3.one/2, transform.position) < 1)
		{
			HidePreview();
			return;
		}

		if (Input.GetButtonDown("Build"))
		{
			_manager.AddBlock(targetPosition);
			HidePreview();
		}
		else
		{
			_blockPreview.transform.position = targetPosition;
		}
	}

	private void HidePreview()
	{
		_blockPreview.transform.position = Vector3.one * 10000;
	}
}