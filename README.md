# About #
Block building playground

### Features ###
* 4 types of blocks with different mining times
* Procedurally generated terrain
* Dynamically loaded/unloaded areas based on player's position
* Persistent saving system

### Controls ###
* Walk - ASDW (or arrows)
* Jump - Space
* Build - Mouse right button
* Mine - Mouse left button
* Pick material - Mouse scroll
* Save game - M
* Load game - L

### Author ###
* Ondřej Paška
* on.paska@gmail.com