﻿using UnityEngine;

public static class CustomExtensions
{
	/**
	 * Same as calling Mathf.Floor on xyz of the vector
	 */
	public static Vector3 Floor(this Vector3 p)
	{
		return new Vector3(Mathf.Floor(p.x),Mathf.Floor(p.y),Mathf.Floor(p.z));
	}
}