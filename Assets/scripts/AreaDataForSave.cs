﻿using System;
/**
 * Container for area data
 * ready to be serialized
 */

[Serializable]
public class AreaDataForSave
{
	public readonly byte[,,] blocks;
	public readonly int X;
	public readonly int Z;

	public AreaDataForSave(byte[,,] b, int x, int z)
	{
		blocks = b;
		X = x;
		Z = z;
	}
}