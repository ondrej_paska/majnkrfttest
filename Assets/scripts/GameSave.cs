﻿using System;
using System.Collections.Generic;
/**
 * Simple container for game save data
 * ready to be serialized
 */
[Serializable]
public class GameSave
{
	public List<AreaDataForSave> Areas;
	public float TerrainOffsetX;
	public float TerrainOffsetY;
	public float PlayerX;
	public float PlayerZ;
}