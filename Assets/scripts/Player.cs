﻿using UnityEngine;

public class Player : MonoBehaviour
{
	private CharacterController _controller;

	[SerializeField] private float _speed = 10;
	[SerializeField] private float _mouseSensitivity = 2;
	[SerializeField] private float _jumpHeight = 1.5f;
	[SerializeField] private LayerMask _ground;
	[SerializeField] private float _groundDistance = 0.36f;
	[SerializeField] private float _gravity = -10f;
	[SerializeField] private Transform _groundChecker;

	private Camera _camera;
	private Vector2 _mouseLook;
	private Vector3 _velocity;

	void Start()
	{
		_camera = GetComponentInChildren<Camera>();
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		_controller = GetComponent<CharacterController>();
	}

	void Update()
	{
		//Built-in "grounded" seems buggy
		var isGrounded =
			Physics.CheckSphere(_groundChecker.position, _groundDistance, _ground, QueryTriggerInteraction.Ignore);

		//====== LOOK AROUND ===========//
		var mouseD = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
		_mouseLook += mouseD * _mouseSensitivity;
		_mouseLook.y = Mathf.Clamp(_mouseLook.y, -80, 80);
		_camera.transform.localRotation = Quaternion.AngleAxis(-_mouseLook.y, Vector3.right);
		transform.localRotation = Quaternion.AngleAxis(_mouseLook.x, Vector3.up);

		//====== MOVE AROUND ===========//
		var move = Vector3.zero;
		move += transform.forward * Input.GetAxis("Vertical") * _speed;
		move += transform.right * Input.GetAxis("Horizontal") * _speed;

		//====== GRAVITY ===========//
		_velocity.y += _gravity * Time.deltaTime;
		if (isGrounded && _velocity.y < 0)
		{
			_velocity.y = 0f;
		}

		//====== JUMP ===========//
		if (Input.GetButtonDown("Jump") && isGrounded)
		{
			_velocity.y += Mathf.Sqrt(_jumpHeight * -2f * _gravity);
		}
		move += _velocity;

		_controller.Move(move * Time.deltaTime);
	}
}